<?php
namespace Helper;

class Helper{
  public static function capitalizedToUnderscored($string){
    return strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $string));
  }
}
