<?php

class UserController extends BaseController {

    public function __construct(){
      $this->beforeFilter('csrf', array('on' => 'post'));
      parent::__construct();
    }
    public function getIndex() {
      return Redirect::to('user/login');
    }

    public function getLogin() {
      if (Auth::check()){
        Session::flash('success', 'You are logged in.');
      }
      $this->layout = 'layout.login_main';
      $this->setTitle('Log In');
      $this->setHeadTitle($this->getTitle());
      $this->addView('form_login_main','form.login_main');
      return $this->compose();
    }

    public function postLogin(){
      $this->layout = 'layout.login_main';
      $this->setTitle('Log In');
      $this->setHeadTitle($this->getTitle());
      $validator = Validator::make(Input::all(), array(
        'email' => 'required|email',
        'password' => 'required',
      ));

      if ($validator->fails()) {
        return Redirect::to('user/login')->withErrors($validator);
      } else {
        $values = Input::all();
        if (Auth::attempt(array('email' => $values['email'], 'password' => $values['password']))) {
          Session::flash('success', 'You are logged in.');
        } else{
          return Redirect::to('user/login')->withErrors($validator);
        }
      }
      $this->addView('form_login_main','form.login_main');
      return $this->compose();
    }
}
