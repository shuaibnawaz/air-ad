<?php

class BaseController extends Controller {

	/**
	 * Page Layout
	 */

	protected $layout = 'layout.default';

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	/**
	 * A collection of child views to be added on the layout.
	 */
	protected $subViews = array();

	/**
	* A collection of variables being exposed in the layout.
	*/
	protected $variables = array();

	/**
	 *	Constructor
	 *
	 * @return void
	 */
	public function __construct(){
		$this->subViews = array();

		$this->variables = array(
			'site_name' => Config::get('app.sitename'),
			'base_path' => base_path(),
			'public_path' => public_path(),
			'app_path' => app_path(),
			'storage_path' => storage_path(),
		);
	}

	/**
	 *
	 */
	public function addView($name, $template, $data=NULL) {
		$this->subViews[$name] = array(
			'template' => $template,
		);

		$this->subViews[$name]['variables'] = array();

		if (! is_null($data)) {
			$this->subViews[$name]['variables'] = $data;
		}
	}

 /**
	* Magic method __call to introduce short dynamic methods
	* to send values to View template.
	* e.g setTitle($value) will appear as $title in the template,
	* setProfilePicture($value) will appear as $profile_picture in the template.
	* @return void
	*/
	public function __call($method, $args){
		$matches = array();
		if(preg_match('/^set((?:[A-Z]{1}[a-z0-9]+)+)$/m', $method, $matches)) {
			$identifier = Helper\Helper::capitalizedToUnderscored($matches[1]);
			$this->variables[$identifier] = $args[0];
		}

		$matches = array();
		if(preg_match('/^get((?:[A-Z]{1}[a-z0-9]+)+)$/m', $method, $matches)) {
			$identifier = Helper\Helper::capitalizedToUnderscored($matches[1]);
			return isset($this->variables[$identifier]) ? $this->variables[$identifier] : NULL;
		}
	}

	/**
	 * Assembles the sub views and variables to be part of primary view that
	 * is being returned for response.
	 *
	 * @return Illuminate\View\View
	 */
	public function compose(){
		if (! is_object($this->layout)) {
				$this->setupLayout();
		}

		foreach($this->subViews as $name => $view) {
			$this->layout->nest($name, $view['template'], $view['variables']);
		}

		foreach($this->variables as $name => $variable) {
			$this->layout->with($name, $variable);
		}

		return $this->layout;
	}

}
