<?php

class PageController extends BaseController {

  public function getIndex() {
    $this->setTitle('Welcome to ' . $this->getSiteName());
    return $this->compose();
  }

  public function getPackage() {
    return $this->compose();
  }

  public function getAboutUs() {
    return $this->compose();
  }

}
