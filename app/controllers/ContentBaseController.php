<?php

class ContentBaseController extends BaseController {
  protected $subViews = array();

  protected $variables = array();

  public function __construct(){
    $this->subViews = array(
      'contentarea' => array(
        'template' => 'region.content',
        'variables' => array('message' => 'This page has no content defined.'),
      ),
    );

    $this->variables = array(
      'site_name' => Config::get('app.sitename'),
      'base_path' => base_path(),
      'public_path' => public_path(),
      'app_path' => app_path(),
      'storage_path' => storage_path(),
    );
  }

  public function __call($method, $args){
    $matches = array();
    if(preg_match('/^set((?:[A-Z]{1}[a-z0-9]+)+)$/m', $method, $matches)) {
      $identifier = Helper\Helper::capitalizedToUnderscored($matches[1]);
      $this->variables[$identifier] = $args[0];
    }

    $matches = array();
    if(preg_match('/^get((?:[A-Z]{1}[a-z0-9]+)+)$/m', $method, $matches)) {
      $identifier = Helper\Helper::capitalizedToUnderscored($matches[1]);
      return $this->variables[$identifier];
    }
  }

  public function compose(){
    foreach($this->subViews as $name => $view) {
      $this->layout->nest($name, $view['template'], $view['variables']);
    }

    foreach($this->variables as $name => $variable) {
      $this->layout->with($name, $variable);
    }

    return $this->layout;
  }

}
