@if($errors->has())
    @foreach ($errors->all() as $error)
        <div>{{ $error }}</div>
    @endforeach
@endif

@if(Session::has('success'))
    <div class="alert-box success">
        <h2>{{ Session::get('success') }}</h2>
    </div>
@endif
{{ Form::open(array('url' => '/user/login', 'id'=> 'form_login_main')) }}
<div class="row collapse">
  <div class="small-12">
    <label class="form-label small-12">Email</label>
    <div class="small-12">
      <input type="text" name="email" class="small-12" placeholder="Email"/>
    </div>
  </div>
  <div class="small-12">
    <label class="form-label small-12">Password</label>
    <div class="small-12">
      <input type="password" name="password" class="small-12" placeholder="Password"/>
    </div>
  </div>
  <div class="small-12">
    <div class="small-12">
      <input type="submit" name="submit" class="small-12" value="Login"/>
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    </div>
  </div>
</div>
{{ Form::close() }}
